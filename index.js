const { PubSub } = require("@google-cloud/pubsub");
const { Subscription } = require("@google-cloud/pubsub/build/src");
const express = require("express");
const app = express();
const port = parseInt(process.env.PORT) || 8080;
app.use(express.json());

const projectId = 'myexpress-beta';
const topicName = 'flash-webhook';
const subscriptionName = 'flash-webhook-mxp-parcel-service';

const pubsub = new PubSub({ projectId });

async function topicExists(topicName) {
    const [topics] = await pubsub.getTopics();
    const topicExists = topics.some(topic => topic.name.endsWith(`/${topicName}`));
    return topicExists;
}
async function createSubscription(topicName, subscriptionName) {
    const topic = pubsub.topic(topicName);
    const [subscription] = await topic.subscription(subscriptionName).get({ autoCreate: true });
    console.log(`Subscription ${subscription.name} created.`);
}

function handleMessage(message) {
    const data = message.data.toString();
    console.log(`Received message: ${data}`);
    message.ack();
}

app.get("/", (req,res) => {
    res.send('Hello World');
});

app.post("/flash/demo-pub", async (req,res) => {
    try {
        const exists = await topicExists(topicName);
        if (!exists) {
            res.status(404).send('Topic not found');
            return;
        }
        await pubsub.topic(topicName).publish(Buffer.from('Test Message!'));
        res.status(200).send('Message published successfully');
    } catch (error) {
        console.error('Error:', error);
        res.status(500).send('Internal Server Error');
    }
});
app.get("/flash/demo-sub", async (req, res) => {
    try {
        const subscription = pubsub.subscription(subscriptionName);
        subscription.on('message', handleMessage);
        
        res.status(200).send('Subscribed to messages');
    } catch (error) {
        console.error('Error:', error);
        res.status(500).send('Internal Server Error');
    }
});

app.listen(port, () => {
  console.log(`Publishing service is running at http://localhost:${port}`);
});
